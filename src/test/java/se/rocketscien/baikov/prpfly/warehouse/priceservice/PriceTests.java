package se.rocketscien.baikov.prpfly.warehouse.priceservice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.testcontainers.junit.jupiter.Testcontainers;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Testcontainers
public class PriceTests {

    static String basicUrl = "http://localhost:{port}/api/v1/price/";
    static String getPriceByProductIdUrl = basicUrl + "product/{productId}";
    static String getPricesByProductIdsUrl = basicUrl + "product";
    static ParameterizedTypeReference<ResponseDto<PriceDto>> PRICE_RESPONSE_TR = new ParameterizedTypeReference<>() {
    };
    static ParameterizedTypeReference<ResponseDto<List<PriceDto>>> PRICES_RESPONSE_TR = new ParameterizedTypeReference<>() {
    };
    static PriceDto price1 = PriceDto.builder()
            .productId(1L)
            .price(BigDecimal.TEN.setScale(4, RoundingMode.HALF_UP))
            .build();
    static PriceDto price2 = PriceDto.builder()
            .productId(2L)
            .price(BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP))
            .build();

    @LocalServerPort
    int port;
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    @DisplayName("Get price by product id - success")
    void testSuccessfulGetPriceByProductId() {
        ResponseDto<PriceDto> response = getPriceByProductId(price1.getProductId());
        assertNotNull(response);
        assertEquals(price1, response.getResult());
        assertNull(response.getError());
    }

    @ParameterizedTest
    @MethodSource("getFailedPriceByProductIdArguments")
    @DisplayName("Get price by product id - fail")
    void testFailedPriceByProductId(
            Object productId,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<PriceDto> response = getPriceByProductId(productId);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> getFailedPriceByProductIdArguments() {
        return Stream.of(
                Arguments.of(
                        999,
                        error(HttpStatus.NOT_FOUND, "Price for product with id [999] not found")
                ),
                Arguments.of(
                        "text",
                        error(HttpStatus.BAD_REQUEST, "Failed to convert " +
                                "value of type 'java.lang.String' to required type 'long'; nested exception is " +
                                "java.lang.NumberFormatException: For input string: \"text\"")
                ),
                Arguments.of(
                        -50,
                        error(HttpStatus.BAD_REQUEST, "Invalid product id [-50]")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("getSuccessfulPricesByProductIdsArguments")
    @DisplayName("Get prices by product ids - success")
    void testSuccessfulGetPricesByProductIds(
            Collection<Long> productIds,
            Collection<PriceDto> expectedResult
    ) {
        ResponseDto<List<PriceDto>> response = getPricesByProductIds(productIds);
        assertNotNull(response);
        assertContainsEqualElements(expectedResult, response.getResult());
        assertNull(response.getError());
    }

    private Stream<Arguments> getSuccessfulPricesByProductIdsArguments() {
        return Stream.of(
                Arguments.of(
                        List.of(price1.getProductId(), price2.getProductId()),
                        List.of(price1, price2)
                ),
                Arguments.of(
                        List.of(999),
                        List.of()
                )
        );
    }

    @ParameterizedTest
    @MethodSource("getFailedPricesByProductIdsArguments")
    @DisplayName("Get prices by product ids - fail")
    void testFailedGetPricesByProductIds(
            Collection<?> productIds,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<List<PriceDto>> response = getPricesByProductIds(productIds);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> getFailedPricesByProductIdsArguments() {
        return Stream.of(
                Arguments.of(
                        List.of(),
                        error(HttpStatus.BAD_REQUEST, "Invalid product ids [[]]")
                ),
                Arguments.of(
                        Arrays.asList(1, null),
                        error(HttpStatus.BAD_REQUEST, "Invalid product id [null]")
                ),
                Arguments.of(
                        List.of("text", 2),
                        error(HttpStatus.BAD_REQUEST, "JSON parse error: Cannot deserialize value of type " +
                                "`java.lang.Long` from String \"text\": not a valid `java.lang.Long` value; " +
                                "nested exception is com.fasterxml.jackson.databind.exc.InvalidFormatException: " +
                                "Cannot deserialize value of type `java.lang.Long` from String \"text\": not a valid `java.lang.Long` value\n" +
                                " at [Source: (org.springframework.util.StreamUtils$NonClosingInputStream); line: 1, column: 2] " +
                                "(through reference chain: java.util.HashSet[0])")
                ),
                Arguments.of(
                        List.of(-50),
                        error(HttpStatus.BAD_REQUEST, "Invalid product id [-50]")
                )
        );
    }

    private ResponseDto<PriceDto> getPriceByProductId(Object productId) {
        return restTemplate
                .exchange(getPriceByProductIdUrl, HttpMethod.GET, HttpEntity.EMPTY, PRICE_RESPONSE_TR, port, productId)
                .getBody();
    }

    private ResponseDto<List<PriceDto>> getPricesByProductIds(Collection<?> productIds) {
        return restTemplate
                .exchange(getPricesByProductIdsUrl, HttpMethod.POST, new HttpEntity<>(productIds), PRICES_RESPONSE_TR, port)
                .getBody();
    }

    private static ResponseDto.ResponseErrorDto error(HttpStatus status, String message) {
        return new ResponseDto.ResponseErrorDto(status.value(), message);
    }

    private static <T> void assertContainsEqualElements(Collection<T> expected, Collection<T> actual) {
        if (expected == null || actual == null) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
        assertTrue(actual.containsAll(expected));
    }

}
