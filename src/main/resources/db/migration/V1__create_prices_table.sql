CREATE TABLE prices
(
    id         BIGSERIAL PRIMARY KEY,
    version    BIGINT         NOT NULL,
    product_id BIGINT         NOT NULL,
    price      NUMERIC(17, 4) NOT NULL
);

CREATE INDEX prices_product_idx ON prices (product_id);