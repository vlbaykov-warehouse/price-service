package se.rocketscien.baikov.prpfly.warehouse.priceservice.price;

import org.mapstruct.Mapper;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;

import java.util.Collection;
import java.util.List;

@Mapper
public interface PriceMapper {

    PriceDto entityToDto(PriceEntity entity);

    List<PriceDto> entitiesToDtos(Collection<PriceEntity> entities);

}
