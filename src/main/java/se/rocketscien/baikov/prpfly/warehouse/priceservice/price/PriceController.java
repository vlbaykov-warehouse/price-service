package se.rocketscien.baikov.prpfly.warehouse.priceservice.price;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/price")
@Tag(name = "Price API")
@RequiredArgsConstructor
@Validated
public class PriceController {

    private final PriceService priceService;
    private final PriceMapper priceMapper;

    @GetMapping(path = "/product/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get price by product ID")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Price not found"),
    })
    public ResponseDto<PriceDto> getPriceByProductId(
            @PathVariable @Parameter(name = "Product ID", required = true) @Min(value = 0, message = "product-id.invalid") long productId
    ) {
        PriceDto result = priceMapper.entityToDto(priceService.getPriceByProductId(productId));
        return ResponseDto.result(result);
    }

    @PostMapping(path = "/product", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get prices by product IDs")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request")
    })
    public ResponseDto<List<PriceDto>> getPricesByProductIds(
            @RequestBody @Parameter(name = "Product IDs", required = true) @NotEmpty(message = "product-ids.invalid") Set<
                    @NotNull(message = "product-id.invalid")
                    @Min(value = 0, message = "product-id.invalid") Long> productIds
    ) {
        return ResponseDto.result(priceMapper.entitiesToDtos(priceService.getPricesByProductIds(productIds)));
    }

}
