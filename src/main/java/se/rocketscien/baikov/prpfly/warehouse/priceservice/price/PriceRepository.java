package se.rocketscien.baikov.prpfly.warehouse.priceservice.price;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface PriceRepository extends CrudRepository<PriceEntity, Long> {

    Optional<PriceEntity> findByProductId(long productId);

    List<PriceEntity> findAllByProductIdIsIn(Collection<Long> productIds);

}
