package se.rocketscien.baikov.prpfly.warehouse.priceservice.price;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class PriceService {

    private final PriceRepository priceRepository;

    public PriceEntity getPriceByProductId(long productId) {
        log.debug("Get price by product id [{}]", productId);
        Optional<PriceEntity> optionalPrice = priceRepository.findByProductId(productId);
        if (optionalPrice.isPresent()) {
            PriceEntity price = optionalPrice.get();
            log.debug("Price found by product id [{}]: [{}]", productId, price);
            return price;
        } else {
            log.debug("Price not found by product id [{}]", productId);
            throw new PriceNotFoundException(productId);
        }
    }

    public List<PriceEntity> getPricesByProductIds(Set<Long> productIds) {
        log.debug("Get prices by product ids [{}]", productIds);
        List<PriceEntity> prices = priceRepository.findAllByProductIdIsIn(productIds);
        log.debug("Price found by product ids [{}]: [{}]", productIds, prices);
        return prices;
    }

}
