package se.rocketscien.baikov.prpfly.warehouse.priceservice.price;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class PriceNotFoundException extends ValidationException {

    public PriceNotFoundException(long productId) {
        super(HttpStatus.NOT_FOUND, "price.not-found", productId);
    }

}
