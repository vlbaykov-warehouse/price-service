https://conf.rocketscien.se/pages/viewpage.action?pageId=20483252

### Введение

Через некоторое время появилась потребность в обогащении данных имеющихся в распоряжении компании “Куркуль”. Менеджмент
принял решение, что для полноты картины не хватает дополнительной информации о товаре: цена, рейтинг.

Для этого нужна интеграция со сторонними поставщиками, но они не торопятся реализовывать API для взаимодействия и кормят
завтраками. Принято решение эмулировать работу сторонней системы, создав свои сервисы, предоставляющие цену и рейтинг.

### Постановка задачи

- Создать price-service на Spring Boot, который будет предоставлять цену товара. Реализация будет состоять из этапов:
    - Реализовать REST API для получения стоимости товара по его идентификатору.
    - На стороне основного сервиса периодически запрашивать цены товаров.
- Создать rating-service на Spring Boot, который будет предоставлять рейтинг товара. Реализация будет состоять из
  этапов:
    - Реализовать предоставление рейтинга путем получения REST запроса (Feign client) с идентификатором товара и
      последующей отправкой рейтинга в очередь Kafka.
    - На стороне основного сервиса периодически запрашивать рейтинг товаров.